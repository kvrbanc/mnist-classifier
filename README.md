# MNIST-classifier

This repository contains code that implements the Classifier service part of the MNIST-classifier system. The system consists of Frontend, Backend, and Classifier service. Git repositories of the remaining parts of the system:
- Frontend: https://gitlab.com/kvrbanc/mnist-classifier-frontend
- Backend: https://gitlab.com/kvrbanc/mnist-classifier-backend

## General notes

The folder *Colab notebooks* contains a **Google colab** notebook using which the AI model was prototyped, trained and validated.

The folder *trained_model_parameters* contains the trained model parameters, and a text file containing the model's evaluation metrics.

The Classifier service uses the AI model with *pre-trained parameters* to make predictions on the digits from the MINST dataset. Also, it has an endpoint which returns a specified number of digits from the MNIST dataset.
