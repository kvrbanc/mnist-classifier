# Base image - Python 3.10.10
FROM bitnami/python:3.10.10

# Create a new working directory and copy application files within it
WORKDIR /usr/src/classifier
COPY app /usr/src/classifier/app
COPY model /usr/src/classifier/model
COPY trained_model_parameters /usr/src/classifier/trained_model_parameters
COPY requirements.txt /usr/src/classifier/requirements.txt

# Create folder to store logs
RUN mkdir logs

# Install necessary python packages from the 'requirements.txt' file
RUN pip3 install --upgrade pip
RUN python3 -m pip install --no-cache-dir --no-input -r requirements.txt && rm requirements.txt

# Set default shell
ENV SHELL /usr/bin/bash

# Port for accessing API server
EXPOSE 8005

# Default shell is BASH
ENV SHELL /bin/bash

# Run the uvicorn server
CMD "uvicorn" "app.main:app" "--host" "0.0.0.0" "--port" "8005"