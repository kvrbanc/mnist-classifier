import numpy as np
from torch.utils.data.dataloader import DataLoader
import torchvision
from typing import List



def get_data_samples(
    *,
    data_loader: DataLoader,
    num_of_samples: int
)-> List[List[List[float]]]:
    """
    Extracts specified number of samples from a dataset

    Arguments:
        data_loader: The dataset from which the samples will be extracted
        num_of_samples: A number specifying the number of samples that need to be extracted
    Returns:
        List[List[List[float]]]: Extracted samples 
    """

    samples = []

    for i, (inputs, _) in enumerate(data_loader):
        inputs = inputs.squeeze()
        samples.append(inputs.numpy().tolist())
        if i == (num_of_samples - 1): break
    
    return samples



def load_dataset(
    *,
    store_path : str
)-> DataLoader:
    """
    Loads the dataset using torchvision package

    Arguments:
        store_path: A path to which the dataset will be stored
    Returns:
        DataLoader: Loaded dataset 
    """
    # Define the dataset
    return DataLoader(torchvision.datasets.MNIST(store_path, 
                                                        train=False,
                                                        download=True,
                                                        transform=torchvision.transforms.Compose([
                                                            torchvision.transforms.ToTensor(),
                                                            torchvision.transforms.Normalize((0.1307,), (0.3081,))
                                                        ])),
                      batch_size=1, shuffle=True)
