import torch
import torch.nn as nn
import torch.nn.functional as F
from typing import (
    Tuple, 
    Union, 
    List
)



def calculate_num_of_input_features(
    *, 
    input_shape : List[int],
    num_of_pooling_layers: int, 
    pooling_kernel: Tuple[int, int],
    num_of_feature_maps: int
) -> int:
  """
  Calculates the number of input features into the network's first fully connected layer

  Arguments:
    input_shape: The shape of the network's input
    num_of_pooling_layers: The number of network's pooling layers
    pooling_kernel: The kernel size of the pooling layers
    num_of_feature_maps: The number of output channels of the network's last convolutional layer
  Returns:
    int: A number of input features into the first fully connected layer
  """
  
  input_shape_after_reduction = [
        input_shape[i+1] // (pooling_kernel[i] ** num_of_pooling_layers) 
        for i in range(len(input_shape) - 1)
  ]
  
  num_of_features = num_of_feature_maps
  for shape in input_shape_after_reduction:
    num_of_features = num_of_features * shape
  
  return num_of_features



class MNISTClassifier(torch.nn.Module):
    """
    Class that implements a neural network using convolutional and fully connected layers

    Attributes:
      layers: The layers of the neural network
    """
    def __init__(
        self,
        *,
        input_shape: List[int] = [1, 28, 28],
        num_of_conv_layers : int = 2,
        num_of_feature_maps : int = 32,
        conv_kernel_size: Union[int, Tuple[int,int]] = (5,5),
        pool_kernel_size: Union[int, Tuple[int,int]] = (2,2),
        dropout_probability: float = 0.4,
        num_of_dense_layers : int = 1,
        sizes_of_dense_layers: List[int] = [128],
        num_of_classes: int = 10
    ):
      """
      Constructor for the class

      Arguments:
        input_shape: The shape of the network's input
        num_of_conv_layers: The number of convolutional layers in the network
        num_of_feature_maps: The number of output channels of the first convolutional layer
        conv_kernel_size: The kernel size of convolutional layers
        pool_kernel_size: The kernel size of pooling layers
        dropout_probability: The probability of zeroing elements on the output of each layer
        num_of_dense_layers: The number of fully connected layers in the network
        sizes_of_dense_layers: The number of neurons in each fully connected layer
        num_of_classes: The number of output classes
      """
      
      super(MNISTClassifier, self).__init__()
      
      self.layers = nn.Sequential()

      # Crete a number of (convolution + Pooling + ReLU + Dropout) layers
      input_channels = 1
      for layer_index in range(num_of_conv_layers):
        input_channels = 1 if layer_index == 0 else num_of_feature_maps
        num_of_feature_maps = num_of_feature_maps * 2 if layer_index > 0 else num_of_feature_maps
        self.layers.append(nn.Conv2d(in_channels=input_channels,
                                     out_channels=num_of_feature_maps,
                                     kernel_size=conv_kernel_size,
                                     padding="same"))
        self.layers.append(nn.MaxPool2d(kernel_size=pool_kernel_size))
        self.layers.append(nn.ReLU())
        self.layers.append(nn.Dropout(p=dropout_probability))

      # Calculate the number of input features into the fully connected layers
      num_of_input_features = calculate_num_of_input_features(
          input_shape=input_shape,
          num_of_pooling_layers=num_of_conv_layers,
          pooling_kernel=pool_kernel_size if isinstance(pool_kernel_size, tuple) else (pool_kernel_size, pool_kernel_size),
          num_of_feature_maps=num_of_feature_maps
      )

      if num_of_input_features == 0:
        raise ValueError(f"Too many (convolution + pooling + ReLU) layers provided. The input of shape {input_shape} gets diminished to zero after {num_of_conv_layers} poolings with kernel {pool_kernel_size}.")

      self.layers.append(nn.Flatten()) 

      # Create a number of fully connected layers 
      for layer_index in range(num_of_dense_layers + 1):
        num_of_input_features = num_of_input_features if layer_index == 0 else num_of_output_features
        num_of_output_features = num_of_classes if layer_index == (num_of_dense_layers) else sizes_of_dense_layers[layer_index]
        self.layers.append(nn.Linear(in_features=num_of_input_features, out_features=num_of_output_features))
        if layer_index < num_of_dense_layers : 
          self.layers.append(nn.ReLU())
          self.layers.append(nn.Dropout(p=dropout_probability))


    def forward(self, x : torch.Tensor) -> torch.Tensor:
        """
        Implements a forward pass through the network

        Arguments:
          x: A model input
        Returns:
          tensor: A model output
        """

        x = self.layers(x)
        return x
    

    def predict_label(self, x : torch.Tensor) -> int:
        """
        Predicts a label for a given input

        Arguments:
          x: A model input
        Returns:
          int: Predicted label
        """

        model_output = self.forward(x)
        label = torch.argmax(F.softmax(model_output, dim=1), dim=1)

        return int(label)





def load_trained_parameters(
    *,
    model: MNISTClassifier,
    path: str
) -> MNISTClassifier:
    """
    Loads model parameters from a file

    Arguments:
        model: A model whose parameters need to be loaded
        path: Path to a file containing model parameters
    Returns:
        MNISTClassifier: A model with loaded parameters
    """
    model.load_state_dict(torch.load(path, map_location=torch.device('cpu')))
    model.eval()

    return model



def get_trained_model(
    *,
    num_of_conv_layers : int = 3,
    num_of_feature_maps : int = 64,
    conv_kernel_size: Union[int, Tuple[int,int]] = (5,5),
    pool_kernel_size: Union[int, Tuple[int,int]] = (2,2),
    dropout_probability: float = 0.4,
    num_of_dense_layers : int = 1,
    sizes_of_dense_layers: List[int] = [128],
    parameter_filepath : str
) -> MNISTClassifier:
    """
      Creates a model and loads model parameters from a file

      Arguments:
        num_of_conv_layers: The number of convolutional layers in the network
        num_of_feature_maps: The number of output channels of the first convolutional layer
        conv_kernel_size: The kernel size of convolutional layers
        pool_kernel_size: The kernel size of pooling layers
        dropout_probability: The probability of zeroing elements on the output of each layer
        num_of_dense_layers: The number of fully connected layers in the network
        sizes_of_dense_layers: The number of neurons in each fully connected layer,
        parameter_filepath: Path to a file containing model parameters
      Returns:
        MNISTClassifier: A model whose parameters are loaded from a file
    """

    # Create a model and load trained model parameters
    model = MNISTClassifier(
            num_of_conv_layers = num_of_conv_layers,
            num_of_feature_maps = num_of_feature_maps,
            conv_kernel_size = conv_kernel_size,
            pool_kernel_size = pool_kernel_size,
            dropout_probability = dropout_probability,
            num_of_dense_layers = num_of_dense_layers,
            sizes_of_dense_layers=sizes_of_dense_layers
        )

    model = load_trained_parameters(model=model, path=parameter_filepath)
    return model
