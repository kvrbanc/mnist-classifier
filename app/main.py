import torch
from fastapi import FastAPI, HTTPException, status
from model import classifier, dataset
from pathlib import Path
from .schemas import PredictLabelRequestSchema, PredictLabelResponseSchema, DataSamplesResponseSchema
from .custom_logging import get_logger



# Constructs paths to the trained model parameters, directory to store data to, and a file to store logs to
root_path = Path(__file__).parent.parent
parameter_path = root_path / "trained_model_parameters" / "trained_parameters.pt"
data_store_path = root_path / "data"
log_store_path = root_path / "logs" / "logfile.log"

# Load logger
logger = get_logger(logger_name=__name__, logging_file=log_store_path)

# Load the trained model
try:
    model = classifier.get_trained_model(parameter_filepath=parameter_path)
except Exception:
    logger.exception("There was a problem with loading the model parameters")

# Load the dataset
try:
    data = dataset.load_dataset(store_path=data_store_path)
except Exception:
    logger.exception("There was a problem with loading the dataset")



app = FastAPI()



@app.get("/api/samples", status_code=status.HTTP_200_OK, response_model=DataSamplesResponseSchema)
async def get_data_samples(num_samples : int = 4):
    """
    Retrieves specified number of samples from the MNIST testing dataset
    """

    samples = []
    # Retrieve samples from the dataset
    try:
        samples = dataset.get_data_samples(
            data_loader=data, 
            num_of_samples=num_samples
        )
    except Exception:
        logger.exception("There was a problem with retrieving samples from the dataset")
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"There was a problem with retrieving samples from the dataset"
        )

    return {
        'data_samples' : samples
    }



@app.post("/api/predict", status_code=status.HTTP_200_OK, response_model=PredictLabelResponseSchema)
async def predict_label(payload: PredictLabelRequestSchema):
    """
    Uses a pre-trained AI model to predict the label for a sample
    """
    
    model_input = payload.input
    model_input = torch.Tensor(model_input)

    # Check input shape
    if tuple(model_input.shape) != (28, 28):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Model input is not of an appropriate shape. Expected shape (28, 28), but shape {tuple(model_input.shape)} was given."
        )

    model_input = torch.reshape(model_input, (1, 1, model_input.shape[0], model_input.shape[1]))
    
    label = -1
    # Use the model to predict the label
    try:
        label = model.predict_label(model_input)
    except Exception:
        logger.exception("There was a problem with using the AI model for inference")
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"There was a problem with using the AI model for inference"
        )

    return {
        "label" : label
    }
    