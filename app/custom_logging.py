import logging



def get_logger(
    *,
    logger_name : str,
    logging_file: str
) -> logging.Logger:
    """
    Creates a logger that logs information to stderr and a file

    Args:
        logger_name: The name of the logger
        logging_file: The path to a file to which to store logs to

    Returns:
        logging.Logger: A logger instance
    """

    # Create a custom logger
    logger = logging.getLogger(logger_name)

    # Create handlers
    console_handler = logging.StreamHandler()
    file_handler = logging.FileHandler(logging_file, mode="a")
    console_handler.setLevel(logging.WARNING)
    file_handler.setLevel(logging.WARNING)

    # Create formatters and add it to handlers
    format = logging.Formatter('%(levelname)s: [%(asctime)s](%(name)s)  %(message)s')

    console_handler.setFormatter(format)
    file_handler.setFormatter(format)

    # Add handlers to the logger
    logger.addHandler(console_handler)
    logger.addHandler(file_handler)

    return logger